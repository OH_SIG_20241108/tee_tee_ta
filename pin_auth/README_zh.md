本可信应用是OpenHarmony中口令验证功能的TEE实现。

请注意：如果本TA要被使用于生产环境中，请重新实现与设备相关的函数： `DeriveDeviceKey()`，位于`pin_auth/src/adaptor/src/adaptor_algorithm.c`；以及`GetDevUdid()`，位于`pin_auth/src/pin_func/pin_auth.c`。